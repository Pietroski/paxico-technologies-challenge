const path = require('path'); // eslint-disable-line

module.exports = {
  mode: 'production',
  devtool: 'eval-source-map',
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        include: [path.resolve(__dirname, 'src')],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@': path.join(__dirname, 'src'),
    },
  },
  output: {
    publicPath: 'build',
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
  },
};
