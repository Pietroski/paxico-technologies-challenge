import { dispatcher } from '@/data/state/simples-state-dispatcher/dispatcher';
import {
  firstDataConjunction,
  secondDataConjunction,
} from './solutions/advanced-solution/index';

import { users, markets } from './data/helper-cases/dataManagement.helper';

import {
  testFirstDataConjunctionTest,
  testSecondDataConjunctionTest,
  testFindWorkplaceByUserId,
  testJuanCarlosWorkplace,
  testWhoIsNotWorking,
  testWhoWorksHard,
} from './tests/index';

function tests(): void {
  console.log('Tests');

  console.group('First Data Conjunction Tests');
  testFirstDataConjunctionTest(users, markets);
  console.groupEnd();

  console.group('Second Data Conjunction Tests');
  testSecondDataConjunctionTest(users, markets);
  console.groupEnd();

  console.group('Find Workplace By User Id Tests');
  testFindWorkplaceByUserId();
  console.groupEnd();

  console.group('Juan Carlos Workplace Tests');
  testJuanCarlosWorkplace();
  console.groupEnd();

  console.group('Who is not working Tests');
  testWhoIsNotWorking();
  console.groupEnd();

  console.group('Who is working hard Tests');
  testWhoWorksHard();
  console.groupEnd();
}

function main(): void {
  /* const firstDataConjunctionList = */ firstDataConjunction(users, markets);
  const secondDataConjunctionList = secondDataConjunction(users, markets);

  dispatcher(secondDataConjunctionList, 'secondDataConjunction');

  tests();
}

main();
console.log('Finished');
