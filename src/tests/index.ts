import testFirstDataConjunctionTest from './first-data-conjunction/testFirstDataConjunction.test';
import testSecondDataConjunctionTest from './second-data-conjunction/testSecondDataConjunction.test';
import testFindWorkplaceByUserId from './find-workplace-by-user-id/testFindWorkplaceByUserId.test';
import testJuanCarlosWorkplace from './test-cases/juan-carlos-workplace/testJuanCarlosWorkplace.test';
import testWhoIsNotWorking from './test-cases/who-is-not-working/testWhoIsNotWorking.test';
import testWhoWorksHard from './test-cases/who-works-hard/testWhoWorksHard.test';

export {
  testFirstDataConjunctionTest,
  testSecondDataConjunctionTest,
  testFindWorkplaceByUserId,
  testJuanCarlosWorkplace,
  testWhoIsNotWorking,
  testWhoWorksHard,
};
