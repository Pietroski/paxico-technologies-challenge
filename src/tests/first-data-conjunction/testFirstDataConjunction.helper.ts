import { NewFirstDataConjunctionType as NFDCT } from '@/data/use-models/dataManagement.types';

const expectedResults: Record<number, NFDCT> = {
  0: {
    name: 'Watermelon Sugar',
    workers: [
      {
        name: 'Matias',
        lastName: 'Parra',
        id: '03la2',
      },
      {
        name: 'Felipe',
        lastName: 'del Toro',
        id: 'K1Ka1u',
      },
      {
        name: 'Juan Carlos',
        lastName: 'Ramboni',
        id: '01slKS',
      },
    ],
    id: 'awi1A1M',
  },
  1: {
    name: 'Sour Diesel',
    workers: [
      {
        name: 'Gustavo',
        lastName: 'Castle',
        id: '20aYLQ',
      },
      {
        name: 'Vin',
        lastName: 'Diesel',
        id: '018sJPQ',
      },
      {
        name: 'Matias',
        lastName: 'Parra',
        id: '03la2',
      },
    ],
    id: 'k193sS',
  },
};

export default expectedResults;
