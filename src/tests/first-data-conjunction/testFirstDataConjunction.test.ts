import {
  UsersType,
  MarketsType,
} from '@/data/helper-cases/dataManagement.helper.types';
import { firstDataConjunction } from '@/solutions/advanced-solution/index';
import { FirstDataConjunctionReturnType as FDCRT } from '@/data/use-models/dataManagement.types';
import expectedResults from './testFirstDataConjunction.helper';

const testFirstDataConjunctionTest = (
  usersList: UsersType,
  marketsList: MarketsType
): void => {
  const iterableExpectation = Object.values(expectedResults);
  const answer: FDCRT = firstDataConjunction(usersList, marketsList);

  iterableExpectation.forEach((e, i: number): void => {
    console.assert(
      e.name === answer[i].name &&
        e.id === answer[i].id &&
        e.workers.every(
          (innerE, j: number): boolean =>
            innerE.name === answer[i].workers[j].name &&
            innerE.lastName === answer[i].workers[j].lastName &&
            innerE.id === answer[i].workers[j].id
        ),
      `Wrong assertation in testFirstDataConjunctionTest at test number ${i}`
    );
  });
};

export default testFirstDataConjunctionTest;
