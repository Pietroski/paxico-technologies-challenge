import { TestType } from './testFindWorkplaceByUserId.types';

const expectedResults: TestType = {
  0: {
    userId: '03la2',
    worksAt: ['Watermelon Sugar', 'Sour Diesel'],
  },
  1: {
    userId: 'J02a1',
    worksAt: [],
  },
  2: {
    userId: 'K1Ka1u',
    worksAt: ['Watermelon Sugar'],
  },
  3: {
    userId: '018sJPQ',
    worksAt: ['Sour Diesel'],
  },
  4: {
    userId: '20aYLQ',
    worksAt: ['Sour Diesel'],
  },
  5: {
    userId: '01slKS',
    worksAt: ['Watermelon Sugar'],
  },
};

export default expectedResults;
