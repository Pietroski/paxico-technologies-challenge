import { findWorkplaceByUserId } from '@/solutions/advanced-solution';
import expectedResults from './testFindWorkplaceByUserId.helper';
import { TestKeyValuePais } from './testFindWorkplaceByUserId.types';

const testFindWorkplaceByUserId: CallableFunction = (): void => {
  const tests = Object.values(expectedResults);

  tests.forEach(({ userId, worksAt }: TestKeyValuePais, index: number) => {
    const results: string[] | [] = findWorkplaceByUserId(userId);

    // mid-level array-comparison...

    // const assertation: boolean = worksAt.every((place: string): boolean =>
    //   results.some((result: string) => place === result)
    // );

    const assertation: boolean =
      JSON.stringify(results) === JSON.stringify(worksAt);

    console.assert(
      assertation,
      `Wrong assertation in testFindWorkplaceByUserId at test number ${index}`
    );

    if (assertation)
      console.log(
        `Assertation passed in testFindWorkplaceByUserId at test number ${index}`
      );
  });
};

export default testFindWorkplaceByUserId;
