export interface TestKeyValuePais {
  userId: string;
  worksAt: string[] | [];
}

export interface TestNumber {
  [testNumber: number]: TestKeyValuePais;
}

export type TestType = TestNumber;
