import {
  MarketsType,
  Name,
} from '@/data/helper-cases/dataManagement.helper.types';

export interface Inputs {
  markets: MarketsType;
}

export type ExpectedOutcomes = Name;

export interface SubTypes {
  inputs: Inputs;
  expectedOutcomes: ExpectedOutcomes[];
}

export interface ExpectedResults {
  [testIndex: number]: SubTypes;
}
