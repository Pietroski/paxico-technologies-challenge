import { juanCarlosWorkplace } from '@/solutions/advanced-solution';
import { markets } from '@/data/helper-cases/dataManagement.helper';
import expectedResults from './testJuanCarlosWorkplace.helper';
import { SubTypes } from './testJuanCarlosWorkplace.helper.types';

const testJuanCarlosWorkplace: CallableFunction = () => {
  const result = juanCarlosWorkplace(markets);
  const testsList: SubTypes[] = Object.values(expectedResults);

  testsList.forEach((e: SubTypes, index: number) => {
    const assertation: boolean =
      JSON.stringify(e.expectedOutcomes) === JSON.stringify(result);

    console.assert(
      assertation,
      `Wrong assertation in testFindWorkplaceByUserId at test number ${index}`
    );

    if (assertation)
      console.log(
        `Assertation passed in testFindWorkplaceByUserId at test number ${index}`
      );
  });
};

export default testJuanCarlosWorkplace;
