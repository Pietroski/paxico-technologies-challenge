import { markets } from '@/data/helper-cases/dataManagement.helper';
import { ExpectedResults } from './testJuanCarlosWorkplace.helper.types';

const expectedResults: ExpectedResults = {
  0: {
    inputs: { markets: [...markets] },
    expectedOutcomes: [
      {
        name: 'Watermelon Sugar',
        id: 'awi1A1M',
      },
    ],
  },
};

export default expectedResults;
