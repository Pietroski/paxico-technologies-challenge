import { users, markets } from '@/data/helper-cases/dataManagement.helper';
import { ExpectedResults } from './testWhoIsNotWorking.helper.types';

const expectedResults: ExpectedResults = {
  0: {
    inputs: { users: [...users], markets: [...markets] },
    expectedOutcomes: [
      {
        name: 'Lizbeth',
        lastName: 'Parra',
        id: 'J02a1',
      },
    ],
  },
};

export default expectedResults;
