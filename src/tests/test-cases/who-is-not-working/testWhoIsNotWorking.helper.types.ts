import {
  UsersType,
  MarketsType,
} from '@/data/helper-cases/dataManagement.helper.types';

export interface Inputs {
  users: UsersType;
  markets: MarketsType;
}

export type ExpectedOutcomes = UsersType;

export interface SubTypes {
  inputs: Inputs;
  expectedOutcomes: ExpectedOutcomes;
}

export interface ExpectedResults {
  [testIndex: number]: SubTypes;
}
