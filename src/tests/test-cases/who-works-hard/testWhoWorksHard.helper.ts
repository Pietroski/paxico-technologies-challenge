import { users, markets } from '@/data/helper-cases/dataManagement.helper';
import { ExpectedResults } from './testWhoWorkdHard.helper.types';

const expectedResults: ExpectedResults = {
  0: {
    inputs: { users: [...users], markets: [...markets] },
    expectedOutcomes: [
      {
        name: 'Matias',
        lastName: 'Parra',
        id: '03la2',
      },
    ],
  },
};

export default expectedResults;
