import { whoWorksHard } from '@/solutions/advanced-solution';
import { users, markets } from '@/data/helper-cases/dataManagement.helper';
import { initialState as newUsers } from '@/data/state';
import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';
import expectedResults from './testWhoWorksHard.helper';
import { SubTypes } from './testWhoWorkdHard.helper.types';

const testWhoWorksHard: CallableFunction = () => {
  const resultLong = whoWorksHard(users, markets);
  const resultShort = whoWorksHard(newUsers.secondDataConjunction as SDCRT);
  const testsList: SubTypes[] = Object.values(expectedResults);

  testsList.forEach((e: SubTypes, index: number) => {
    const assertation: boolean =
      JSON.stringify(e.expectedOutcomes) === JSON.stringify(resultLong);

    console.group('Who is not Working Long Function Tests');

    console.assert(
      assertation,
      `Wrong assertation in testFindWorkplaceByUserId at test number ${index}`
    );

    if (assertation)
      console.log(
        `Assertation passed in testFindWorkplaceByUserId at test number ${index}`
      );

    console.groupEnd();
  });

  testsList.forEach((e: SubTypes, index: number) => {
    const assertation: boolean =
      JSON.stringify(e.expectedOutcomes) === JSON.stringify(resultShort);

    console.group('Who is not Working Short Function Tests');

    console.assert(
      assertation,
      `Wrong assertation in testFindWorkplaceByUserId at test number ${index}`
    );

    if (assertation)
      console.log(
        `Assertation passed in testFindWorkplaceByUserId at test number ${index}`
      );

    console.groupEnd();
  });
};

export default testWhoWorksHard;
