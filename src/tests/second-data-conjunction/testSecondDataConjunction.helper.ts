import { NewSecondDataConjunctionType as NSDCT } from '@/data/use-models/dataManagement.types';

const expectedResults: Record<number, NSDCT> = {
  0: {
    name: 'Matias',
    lastName: 'Parra',
    fullName: 'Matias Parra',
    id: '03la2',
    markets: [
      {
        name: 'Watermelon Sugar',
        id: 'awi1A1M',
      },
      {
        name: 'Sour Diesel',
        id: 'k193sS',
      },
    ],
  },
  1: {
    name: 'Lizbeth',
    lastName: 'Parra',
    fullName: 'Lizbeth Parra',
    id: 'J02a1',
    markets: [],
  },
  2: {
    name: 'Felipe',
    lastName: 'del Toro',
    fullName: 'Felipe del Toro',
    id: 'K1Ka1u',
    markets: [
      {
        name: 'Watermelon Sugar',
        id: 'awi1A1M',
      },
    ],
  },
  3: {
    name: 'Vin',
    lastName: 'Diesel',
    fullName: 'Vin Diesel',
    id: '018sJPQ',
    markets: [
      {
        name: 'Sour Diesel',
        id: 'k193sS',
      },
    ],
  },
  4: {
    name: 'Gustavo',
    lastName: 'Castle',
    fullName: 'Gustavo Castle',
    id: '20aYLQ',
    markets: [
      {
        name: 'Sour Diesel',
        id: 'k193sS',
      },
    ],
  },
  5: {
    name: 'Juan Carlos',
    lastName: 'Ramboni',
    fullName: 'Juan Carlos Ramboni',
    id: '01slKS',
    markets: [
      {
        name: 'Watermelon Sugar',
        id: 'awi1A1M',
      },
    ],
  },
};

export default expectedResults;
