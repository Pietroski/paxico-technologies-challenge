import {
  UsersType,
  MarketsType,
} from '@/data/helper-cases/dataManagement.helper.types';
import { secondDataConjunction } from '@/solutions/advanced-solution/index';
import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';
import expectedResults from './testSecondDataConjunction.helper';

const testSecondDataConjunctionTest = (
  usersList: UsersType,
  marketsList: MarketsType
): void => {
  const iterableExpectation = Object.values(expectedResults);
  const answer: SDCRT = secondDataConjunction(usersList, marketsList);

  iterableExpectation.forEach((e, i: number): void => {
    console.assert(
      e.name === answer[i].name &&
        e.lastName === answer[i].lastName &&
        e.fullName === answer[i].fullName &&
        e.id === answer[i].id &&
        e.markets.every(
          (innerE, j: number): boolean =>
            innerE.name === answer[i].markets[j].name &&
            innerE.id === answer[i].markets[j].id
        ),
      `Wrong assertation in testSecondDataConjunctionTest at test number ${i}`
    );
  });
};

export default testSecondDataConjunctionTest;
