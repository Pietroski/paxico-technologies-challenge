import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';

enum EActionTypes {
  FIRT_CONJUNCTION,
  SECOND_CONJUNCTION,
}

interface ActionType {
  type: EActionTypes;
  payload: SDCRT;
}

const secondDataConjunctionAction: CallableFunction = (
  newList: SDCRT
): ActionType => ({
  type: EActionTypes.SECOND_CONJUNCTION,
  payload: [...newList],
});

export default secondDataConjunctionAction;
