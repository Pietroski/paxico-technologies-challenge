const dispatch: CallableFunction = (func: FunctionStringCallback) => func;

export default dispatch;
