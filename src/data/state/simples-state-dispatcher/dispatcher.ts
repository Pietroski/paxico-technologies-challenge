import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';

interface StateType {
  [keyName: string]: SDCRT;
}

export let initialState: StateType = {} as StateType; // eslint-disable-line

export const dispatcher = (dispatchableState: SDCRT, keyName: string): void => {
  initialState = { [keyName]: { ...dispatchableState } };
};
