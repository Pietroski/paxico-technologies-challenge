import {} from '@/data/helper-cases/dataManagement.helper.types';
import {} from '@/data/use-models/dataManagement.types';

const initialState = {};

// eslint-disable-next-line
const conjunctionReducer = (state = initialState, action: any): any => {
  switch (action) {
    case action.test:
      return state;
    default:
      return state;
  }
};

export default conjunctionReducer;
