import { UsersType, MarketsType } from './dataManagement.helper.types';

export const users: UsersType = [
  {
    name: 'Matias',
    lastName: 'Parra',
    id: '03la2',
  },
  {
    name: 'Lizbeth',
    lastName: 'Parra',
    id: 'J02a1',
  },
  {
    name: 'Felipe',
    lastName: 'del Toro',
    id: 'K1Ka1u',
  },
  {
    name: 'Vin',
    lastName: 'Diesel',
    id: '018sJPQ',
  },
  {
    name: 'Gustavo',
    lastName: 'Castle',
    id: '20aYLQ',
  },
  {
    name: 'Juan Carlos',
    lastName: 'Ramboni',
    id: '01slKS',
  },
];

export const markets: MarketsType = [
  {
    name: 'Watermelon Sugar',
    workers: ['03la2', 'K1Ka1u', '01slKS'],
    id: 'awi1A1M',
  },
  {
    name: 'Sour Diesel',
    workers: ['20aYLQ', '018sJPQ', '03la2'],
    id: 'k193sS',
  },
];
