export interface Name {
  name: string;
  id: string;
}

export interface IUsers extends Name {
  lastName: string;
}

export interface IMarkets extends Name {
  workers: Array<string>;
}

export type UsersType = Array<IUsers>;
export type MarketsType = Array<IMarkets>;
