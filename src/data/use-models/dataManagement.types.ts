import {
  UsersType,
  Name,
} from '@/data/helper-cases/dataManagement.helper.types';

export interface NewFirstDataConjunctionType {
  name: string;
  workers: UsersType;
  id: string;
}

export interface NewSecondDataConjunctionType {
  name: string;
  lastName: string;
  fullName: string;
  id: string;
  markets: Name[];
}

export type FirstDataConjunctionReturnType = Array<NewFirstDataConjunctionType>;
export type SecondDataConjunctionReturnType = Array<NewSecondDataConjunctionType>;
