import {
  UsersType,
  MarketsType,
} from '@/data/helper-cases/dataManagement.helper.types';
import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';
import whoIsNotWorkingLong from './whoIsNotWorkingLong';
import whoIsNotWorkingShort from './whoIsNotWorkingShort';

const parameterChecker = (f: UsersType | SDCRT, s?: MarketsType): number => {
  if ((f as UsersType) && (s as MarketsType)) return 2;
  return 1;
};

// Since JavaScript does not support method overloading such as Java and/or C#...
const whoIsNotWorking: CallableFunction = (
  usersList: UsersType | SDCRT,
  marketsList?: MarketsType
): UsersType => {
  const argsLength = parameterChecker(usersList, marketsList);

  if (argsLength === 2) {
    const notWorkingListFromLong: UsersType = whoIsNotWorkingLong(
      usersList,
      marketsList as MarketsType
    );

    return notWorkingListFromLong;
  }

  const notWorkingListFromShort: UsersType = whoIsNotWorkingShort(
    usersList as SDCRT
  );
  return notWorkingListFromShort;
};

export default whoIsNotWorking;
