import { UsersType } from '@/data/helper-cases/dataManagement.helper.types';
import {
  SecondDataConjunctionReturnType as SDCRT,
  NewSecondDataConjunctionType as NSDCT,
} from '@/data/use-models/dataManagement.types';

const whoIsNotWorkingShort = (usersObjectList: SDCRT): UsersType => {
  const usersList = Object.values(usersObjectList);

  // This method sorts alphabetically naturally...
  // const users: SDCRT = usersList.filter(
  //   (user: NSDCT) => user.markets.length === 0
  // );

  // With reduce method I can not only order the way I want the keys but also select which keys I want...
  const users: UsersType = usersList.reduce(
    (acc: UsersType, user: NSDCT) =>
      (user.markets.length === 0 && [
        ...acc,
        { name: user.name, lastName: user.lastName, id: user.id },
      ]) || [...acc],
    [] as UsersType
  );

  console.log(`Users that are not working anywhere -> `, users);

  return users;
};

export default whoIsNotWorkingShort;
