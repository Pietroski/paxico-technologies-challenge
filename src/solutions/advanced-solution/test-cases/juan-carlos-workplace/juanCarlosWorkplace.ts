import {
  IMarkets,
  MarketsType,
  Name,
} from '@/data/helper-cases/dataManagement.helper.types';

const JUAN_CARLOS = {
  name: 'Juan Carlos',
  lastName: 'Ramboni',
  id: '01slKS',
};

const juanCarlosWorkplace: CallableFunction = (
  marketsList: MarketsType
): Name[] => {
  // This method return an array with the ID property before the name in each Object element -> due to natural sorting;

  // const workplaces = marketsList.filter((market: IMarkets) =>
  //   market.workers.some((workerId: string) => workerId === JUAN_CARLOS.id)
  // );

  // This methods garantees that the  name stays before the ID property of the Object element...

  const workplaces = marketsList.reduce(
    (acc: Name[], market: IMarkets): Name[] =>
      (market.workers.some(
        (workerId: string) => workerId === JUAN_CARLOS.id
      ) && [...acc, { name: market.name, id: market.id }]) ||
      acc,
    [] as Name[]
  );

  console.log('Juan Carlos works at -> ', workplaces);

  return workplaces;
};

export default juanCarlosWorkplace;
