import {
  UsersType,
  MarketsType,
} from '@/data/helper-cases/dataManagement.helper.types';
import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';
import whoWorksHardLong from './whoWorksHardLong';
import whoWorksHardShort from './whoWorksHardShort';

const parameterChecker = (f: UsersType | SDCRT, s?: MarketsType): number => {
  if ((f as UsersType) && (s as MarketsType)) return 2;
  return 1;
};

// Since JavaScript does not support method overloading such as Java and/or C#...
const whoWorksHard: CallableFunction = (
  usersList: UsersType | SDCRT,
  marketsList?: MarketsType
): UsersType => {
  const argsLength = parameterChecker(usersList, marketsList);

  if (argsLength === 2) {
    const notWorkingListFromLong: UsersType = whoWorksHardLong(
      usersList,
      marketsList as MarketsType
    );

    return notWorkingListFromLong;
  }

  const notWorkingListFromShort: UsersType = whoWorksHardShort(
    usersList as SDCRT
  );
  return notWorkingListFromShort;
};

export default whoWorksHard;
