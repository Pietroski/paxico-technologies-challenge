import {
  UsersType,
  MarketsType,
  IUsers,
  IMarkets,
} from '@/data/helper-cases/dataManagement.helper.types';

const whoWorksHardLong = (
  usersList: UsersType,
  marketsList: MarketsType
): UsersType => {
  // To apply a selective and already ordered key-value pair object...
  const users: UsersType = usersList.reduce(
    (acc: UsersType, user: IUsers) =>
      (marketsList.every((market: IMarkets): boolean =>
        market.workers.some((workerId: string): boolean => workerId === user.id)
      ) && [...acc, { ...user }]) || [...acc],
    [] as UsersType
  );

  console.log(`Users that work in more than one place -> `, users);

  return users;
};

export default whoWorksHardLong;
