import firstDataConjunction from './first-data-conjunction/firstDataConjunction';
import secondDataConjunction from './second-data-conjunction/secondDataConjunction';
import findWorkplaceByUserId from './find-workplace-by-user-id/findWorldplaceByUserId';
import juanCarlosWorkplace from './test-cases/juan-carlos-workplace/juanCarlosWorkplace';
import whoIsNotWorking from './test-cases/who-is-not-working/whoIsNotWorking';
import whoWorksHard from './test-cases/who-works-hard/whoWorksHard';

export {
  firstDataConjunction,
  secondDataConjunction,
  findWorkplaceByUserId,
  juanCarlosWorkplace,
  whoIsNotWorking,
  whoWorksHard,
};
