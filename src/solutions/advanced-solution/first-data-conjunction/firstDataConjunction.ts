import {
  UsersType,
  MarketsType,
  IUsers,
  IMarkets,
} from '../../../data/helper-cases/dataManagement.helper.types';
import { FirstDataConjunctionReturnType as FDCRT } from '../../../data/use-models/dataManagement.types';

const firstDataConjunction: CallableFunction = (
  usersList: UsersType,
  marketsList: MarketsType
): FDCRT => {
  const conjunction = marketsList.reduce(
    (acc: FDCRT, market: IMarkets): FDCRT => [
      ...acc,
      {
        name: market.name,
        workers: market.workers.reduce(
          (innerAcc: IUsers[], workerId: string): IUsers[] => [
            ...innerAcc,
            ...usersList.reduce(
              (a: UsersType, { name, lastName, id }: IUsers): UsersType =>
                (workerId === id && [
                  ...a,
                  {
                    name,
                    lastName,
                    id,
                  },
                ]) || [...a],
              [] as UsersType
            ),
          ],
          [] as IUsers[]
        ),
        id: market.id,
      },
    ],
    [] as FDCRT
  );

  console.log(conjunction);
  return conjunction;
};

export default firstDataConjunction;
