import {
  UsersType,
  MarketsType,
  IUsers,
  Name,
} from '@/data/helper-cases/dataManagement.helper.types';
import { SecondDataConjunctionReturnType as SDCRT } from '@/data/use-models/dataManagement.types';

const secondDataConjunction: CallableFunction = (
  usersList: UsersType,
  marketsList: MarketsType
): SDCRT => {
  const conjunction = usersList.reduce(
    (acc: SDCRT, user: IUsers) => [
      ...acc,
      {
        name: user.name,
        lastName: user.lastName,
        fullName: `${user.name} ${user.lastName}`,
        id: user.id,
        markets: marketsList.reduce(
          (a: Name[], market): Name[] =>
            (market.workers.some(
              (workerId: string) => workerId === user.id
            ) && [...a, { name: market.name, id: market.id }]) || [...a],
          [] as Name[]
        ),
      },
    ],
    [] as SDCRT
  );

  console.log(conjunction);
  return conjunction;
};

export default secondDataConjunction;
