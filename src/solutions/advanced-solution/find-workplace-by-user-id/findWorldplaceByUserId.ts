import { users, markets } from '@/data/helper-cases/dataManagement.helper';
import { secondDataConjunction } from '@/solutions/advanced-solution';
import { NewSecondDataConjunctionType as NSDCT } from '@/data/use-models/dataManagement.types';
import { IMarkets } from '@/data/helper-cases/dataManagement.helper.types';

const findWorkplaceByUserId: CallableFunction = (
  userId: string
): string | string[] => {
  const usersListWithWorkplace = secondDataConjunction(users, markets);

  const foundUser = usersListWithWorkplace.filter(
    (e: NSDCT): boolean => e.id === userId
  )[0];

  if (!foundUser) {
    console.log(`The user with the id: ${userId} does not exist!`);
    return `The user with the id: ${userId} does not exist!`;
  }

  const workplaceNumber: number = foundUser.markets.length;
  if (workplaceNumber === 0)
    console.log(`The user with the id: ${userId} does not work anywhere`);

  const marketsList = foundUser.markets.map(
    (market: IMarkets): string => market.name
  );

  console.log(`The user with the id: ${userId} works at`, marketsList);
  return marketsList;

  // Advanced stringification/concatenation method...

  // const workplaceNumber: number = foundUser.markets.length;
  // if (workplaceNumber === 0) return `The user does not work anywhere`;
  // const compair = workplaceNumber - 1;
  // return `The user works at: ${foundUser.markets.reduce(
  //   (acc: string, market: string, index: number) =>
  //     (index === 0 && `${acc} ${market}, `) ||
  //     (index > 0 && index < compair && `${acc}, ${market}, `) ||
  //     (index === compair && `${acc}, ${market}.`) ||
  //     acc,
  //   '' as string
  // )}`;
};

export default findWorkplaceByUserId;
