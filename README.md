# PAXICO TECHNOLOGIES' CHALLENGE

### RUNNING THE CODE CHALLENGE

- The challenge was built upon Node 15.4.0, so bare that in mind whilst executing it in the terminal only. For that reason I have created a `index.html` file in the ./public directory so the console.logs can be seen in the browser much easier and with all the data details.

- Bare in mind that the console.logs prints are limited whilst running in the terminal console. Therefore, there is an `index.html` file in the ./public directory for this purpose that handles data details much better.

- To run the code via terminal, simply run `$ npm install` with node 15 or later and then run `$ npm run run`.

- Please just open the `index.html` file located inside the ./public folder.

- WARNING!! In order for you to be able to see the console.log results you should clone the application into your local environment.

### Further considerations

- I had no difficulty solving the code challenge at all. Therefore I have chosen to beautify it in a more robust way and architecture. I used TypeScript alonsige JS and chose to build my own webpack config file. It was working fine until later on when the conditional chaining operator stopped working on the node environment and I had to drop it even though it would work perfectly fine on the web browser runtime. 

- Besides TypeScript I have also installed and configured Eslint and Prettier to keep my code well organised and more readable.

- I have created 3 main solution folders but due to other factors I decided just to keep my advanced solution, hence it is not copyable and tells that it is unique and I did it alone. I can write simpler and more intermediate ways of solving this code challenge if you think so... 

- I had developed manual tests before and/or in conjunction with the code solutions.

- I was thinking about creating a docker file to run it locally but I decided it was too much for too little. Besides, that is why I also have created an `index.html` file.

- I had some late problems with gitlab deployment hence it is not being able to access the bundle.js in the build directory. That is the explanation why you will have to clone the repository and open the `index.html` locally. Sorry for that. =/

- The code repository is with public visibility so you can see and read the code... it is in this link: [Gitlab Code Challenge Repository's Link](https://gitlab.com/Pietroski/paxico-technologies-challenge)

- Please, any doubts or considerations you let me know!!

- I hope you enjoy looking through it and reach me out as soon as possible.

Best Regards,

Augusto Pietroski
