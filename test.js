const users = [
  {
    name: 'Matias',
    lastName: 'Parra',
    id: '03la2',
  },
  {
    name: 'Lizbeth',
    lastName: 'Parra',
    id: 'J02a1',
  },
  {
    name: 'Felipe',
    lastName: 'del Toro',
    id: 'K1Ka1u',
  },
  {
    name: 'Vin',
    lastName: 'Diesel',
    id: '018sJPQ',
  },
  {
    name: 'Gustavo',
    lastName: 'Castle',
    id: '20aYLQ',
  },
  {
    name: 'Juan Carlos',
    lastName: 'Ramboni',
    id: '01slKS',
  },
];

const markets = [
  {
    name: 'Watermelon Sugar',
    workers: ['03la2', 'K1Ka1u', '01slKS'],
    id: 'awi1A1M',
  },
  {
    name: 'Sour Diesel',
    workers: ['20aYLQ', '018sJPQ', '03la2'],
    id: 'k193sS',
  },
];

/*
  - You must make a conjunction with the data of the markets and users,
  as result, in each index of worker instead of a string with the user id you will
  show the object of the user that is present in the users array with the given id
  Model Result:
  {
    name: "My Dummy Market",
    workers: [
        {
            name: "Dummy User Name",
            lastName: "Dummy LastName",
            id: "Dummy Id"
        }
    ],
    id: "Dummy Id"
  }
  - You must make a conjunction with the data of the workers and markets,
  as result, in each user you will show the data of the market that is present
  in the markets array with the given id and adding, as an extra field, to the user,
  a field called fullName that is the concat of the name, the last name and an middle string
  with a empty space
  Model Result:
  {
    name: "Dummy User Name",
    lastName: "Dummy LastName",
    fullName: "Dummy User Name Dummy LastName",
    id: "Dummy Id",
    markets: [
       {
          name: "Watermelon Sugar",
          id: "awi1A1M"
       }
    ]
  }
  - Create a function that receives a user id and returns the market where he belongs
    
  Test Cases: 
  - Where does Juan Carlos Ramboni work?
  - Which worker does not work in any market?
  - Which worker does work in more than one market?
  
  Notes:
  - You must show in the console all the data
*/
